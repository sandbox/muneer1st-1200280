<?php
/**
 * @file
 * The forms file, which contains the form generation and execution
 * for the administration section of the banner.
 */

/**
 * Implementation of ez_banner_rotator_get_form_add_banner_submit().
 */
function ez_banner_rotator_get_form_add_banner_submit($form, &$form_state) {

  $bid = 0;

  try {
    $bid = (int) $form_state['values']['bid'];
  }
  catch (Exception $ex) {
    throw new Exception($ex->getMessage());
  }

  $form_values = $form_state['values'];
  foreach ($form_values as $key => $values) {
    $form_values[$key] = check_plain($values);
  }

  $options = array(
    'transition_type'         => $form_values['transition_type'],
    'auto_rotate'             => $form_values['auto_rotate'],
    'auto_rotate_timeout'     => $form_values['auto_rotate_timeout'],
    'enable_footer'           => $form_values['enable_footer'],
    'auto_hide_footer'        => $form_values['auto_hide_footer'],
    'enable_thumb_image_link' => $form_values['enable_thumb_image_link'],
    'enable_title'            => $form_values['enable_title'],
    'enable_description'      => $form_values['enable_description'],
    'enable_readmore'         => $form_values['enable_readmore'],
  );

  $values = array(
    'title'       => $form_values['title'],
    'description' => $form_values['description'],
    'width'       => $form_values['width'],
    'height'      => $form_values['height'],
    'num_images'  => $form_values['num_images'],
    'other_options' => serialize($options),
  );

  $num_updated = $id = 0;

  if ($bid > 0) {
    $num_updated = db_update('ez_banner_rotator_blocks')
      ->fields($values)
      ->condition('bid', $bid)
      ->execute();
  }
  else {
    $id = db_insert('ez_banner_rotator_blocks')
        ->fields($values)
        ->execute();
  }

  // Show message.
  if ($id > 0 || $num_updated) {
    $saved_item = $form_values['title'];
    drupal_set_message($saved_item . ' ' . t('block has been saved.'));
  }

  drupal_goto('admin/structure/ez-banner-blocks');
}

/**
 * Implementation of ez_banner_rotator_get_form_add_banner_validate().
 */
function ez_banner_rotator_get_form_add_banner_validate($form, &$form_state) {
  // Validating width.
  $is_numeric_width = is_numeric($form_state['values']['width']);
  if (!$is_numeric_width) {
    form_set_error('width', t('"Width" must be a numeric value.'));
  }
  elseif ($form_state['values']['width'] < 200) {
    form_set_error('width', t('"Width" is too small to proceed.'));
  }

  // Validating height.
  $is_numeric_height = is_numeric($form_state['values']['height']);
  if (!$is_numeric_height) {
    form_set_error('height', t('"Height" must be a numeric value.'));
  }
  elseif ($form_state['values']['height'] < 100) {
    form_set_error('height', t('"Height" is too small to proceed.'));
  }

  // Validating number of images.
  $is_numeric_num_images = is_numeric($form_state['values']['num_images']);
  if (!$is_numeric_num_images) {
    form_set_error('num_images', t('"Number of Images" must be a numeric value.'));
  }
  elseif (2 > $form_state['values']['num_images'] || 10 < $form_state['values']['num_images']) {
    form_set_error('num_images', t('"Number of Images" must be at least 2 and should not be more than 10.'));
  }
}

/**
 * Implementation of _get_form_add_banner().
 */
function ez_banner_rotator_get_form_add_banner($form, &$form_state, $args = '') {
  $other_options = array();
  if (is_object($args)) {
    $other_options = $args->other_options;
  }
  else {
    // Load the banner object.
    module_load_include('inc', 'ez_banner_rotator', 'ez_banner_rotator.banner');
    $tmp_obj = new EZBannerRotator();
    $args = new stdClass();
    $args->width = $tmp_obj->width;
    $args->height = $tmp_obj->height;
    $args->num_images = $tmp_obj->numOfImages;
    $other_options['transition_type']    = $tmp_obj->transitionType;
    $other_options['auto_rotate']        = $tmp_obj->autoRotate;
    $other_options['auto_rotate_timeout'] = $tmp_obj->autoRotateTimeout;
    $other_options['enable_footer']      = $tmp_obj->enableFooter;
    $other_options['auto_hide_footer']   = $tmp_obj->autoHideFooter;
    $other_options['enable_title']       = $tmp_obj->enableTitle;
    $other_options['enable_thumb_image_link'] = $tmp_obj->enableThumbImageLink;
    $other_options['enable_description'] = $tmp_obj->enableDescription;
    $other_options['enable_readmore']    = $tmp_obj->enableReadmore;
  }

  $form['block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block Information'),
  );
  $form['bid'] = array(
    '#type' => 'value',
    '#value' => !empty($args->bid) ? $args->bid : '0',
  );
  $form['block']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 30,
    '#default_value' => !empty($args->title) ? $args->title : '',
    '#required' => TRUE,
  );
  $form['block']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#size' => 20,
    '#default_value' => !empty($args->width) ? $args->width : '',
    '#required' => TRUE,
    '#description' => t('Enter width in pixel. Provide a numeric value larger than 200.'),
  );
  $form['block']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#size' => 20,
    '#default_value' => !empty($args->height) ? $args->height : '',
    '#required' => TRUE,
    '#description' => t('Enter height in pixel. Provide a numeric value larger than 100.'),
  );
  $form['block']['num_images'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Images'),
    '#size' => 20,
    '#default_value' => !empty($args->num_images) ? $args->num_images : 0,
    '#required' => TRUE,
    '#description' => t('Enter number of images to display in this banner block. Min = 2, Max = 10'),
  );

  $transition_type_default_val = 'slide';
  if (!empty($other_options['transition_type'])) {
    $transition_type_default_val = $other_options['transition_type'];
  }

  $form['block']['transition_type'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#default_value' => $transition_type_default_val,
    '#required' => TRUE,
    '#description' => t('Choose the style of banner animation.'),
    '#options' => array(
      'slide' => 'Slide Horizontal',
      'slideDown' => 'Slide Vertical',
      'fade' => 'Fade',
      'random' => 'Random',
    ),
  );
  $form['block']['optons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['block']['optons']['auto_rotate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto Rotate'),
    '#default_value' => $other_options['auto_rotate'],
    '#description' => t('Enable auto banner transition.'),
  );
  $form['block']['optons']['auto_rotate_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Auto Rotation Timeout'),
    '#size' => 30,
    '#default_value' => $other_options['auto_rotate_timeout'],
    '#required' => TRUE,
    '#description' => t('Timeout value in milliseconds.'),
  );
  $form['block']['optons']['enable_footer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Banner Footer bar'),
    '#default_value' => $other_options['enable_footer'],
    '#description' => t('Enable / Disable the footer bar of the banner.'),
  );
  $form['block']['optons']['auto_hide_footer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autohide Footer bar'),
    '#default_value' => $other_options['auto_hide_footer'],
    '#description' => t("Enable / Disable the footer bar's autohide feature."),
  );
  $form['block']['optons']['enable_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Title in Banner Footer bar'),
    '#default_value' => $other_options['enable_title'],
    '#description' => t('Enable / Disable Title in the footer bar of the banner.'),
  );
  $form['block']['optons']['enable_thumb_image_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Thumbnail image link'),
    '#default_value' => $other_options['enable_thumb_image_link'],
    '#description' => t('Enable / Disable link for the thumbnail image in Banner footer bar.'),
  );
  $form['block']['optons']['enable_description'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Banner Footer description'),
    '#default_value' => $other_options['enable_description'],
    '#description' => t('Enable / Disable description in Banner footer bar.'),
  );
  $form['block']['optons']['enable_readmore'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Banner Footer Readmore Button'),
    '#default_value' => $other_options['enable_readmore'],
    '#description' => t('Enable / Disable readmore button in Banner footer bar.'),
  );
  $form['block']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#size' => 60,
    '#default_value' => !empty($args->description) ? $args->description : '',
  );
  if (!empty($args->bid) && $args->bid > 0) {
    $form['block']['update'] = array(
      '#type' => 'submit',
      '#value' => t('Update Block'),
    );
  }
  else {
    $form['block']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Block'),
    );
  }
  return $form;
}

/**
 * Implementation of ez_banner_rotator_get_form_edit_banner().
 */
function ez_banner_rotator_get_form_edit_banner($arg1) {
  $query = db_select('ez_banner_rotator_blocks', 'ez');
  $query->fields('ez');
  $query->condition('ez.bid', $arg1, '=');
  $result = $query->execute()->fetch();

  $result->other_options = unserialize($result->other_options);

  return drupal_get_form('ez_banner_rotator_get_form_add_banner', $result);
}


/**
 * Implementation of ez_banner_rotator_get_form_delete_banner().
 */
function ez_banner_rotator_get_form_delete_banner($form, &$form_state, $arg1) {
  $bid = 0;

  try {
    $bid = (int) $arg1;
  }
  catch (Exception $ex) {
    drupal_set_message($ex->getMessage());
    drupal_goto('admin/structure/ez-banner-blocks');
  }

  $query = db_select('ez_banner_rotator_blocks', 'ez');
  $query->fields('ez');
  $query->condition('ez.bid', $bid, '=');
  $result = $query->execute()->fetch();

  $question = t('Are you sure you want to delete the block');
  $question .= '<em>"' . check_plain($result->title) . '"?</em>';

  $description = t('This action can not be undone.');

  $form['bid'] = array(
    '#type' => 'value',
    '#value' => $bid,
  );

  return confirm_form($form, $question, 'admin/structure/ez-banner-blocks',
    $description, t('Delete'), t('Cancel'));
}

/**
 * Implementation of ez_banner_rotator_get_form_delete_banner_submit().
 */
function ez_banner_rotator_get_form_delete_banner_submit($form, &$form_state) {
  $num_deleted = db_delete('ez_banner_rotator_blocks')
      ->condition('bid', $form_state['values']['bid'])
      ->execute();

  if ($num_deleted > 0) {
    drupal_set_message(t('The block has been deleted.'));
  }

  drupal_goto('admin/structure/ez-banner-blocks');
}


/**
 * Implementation of ez_banner_rotator_get_form_add_banner_layer().
 */
function ez_banner_rotator_get_form_add_banner_layer($form, $form_state, $arg1, $arg2) {

  drupal_add_css(drupal_get_path('module', 'ez_banner_rotator') . '/styles/admin.css');

  if (empty($arg1) || !is_numeric($arg1)) {
    $arg1 = 0;
  }
  if (empty($arg2) || !is_numeric($arg2)) {
    $arg2 = 0;
  }

  $ezr = new stdClass();

  if ($arg2 > 0) {
    $ezr = db_select('ez_banner_rotator', 'ezr')
      ->fields('ezr')
      ->condition('ezr.image_id', $arg2)
      ->execute()->fetch();
  }

  $form['block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update Banner Layer'),
  );

  $form['bid'] = array(
    '#type' => 'value',
    '#value' => $arg1,
  );

  $form['image_id'] = array(
    '#type' => 'value',
    '#value' => $arg2,
  );

  $form['block']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 30,
    '#default_value' => !empty($ezr->title) ? $ezr->title : '',
    '#required' => TRUE,
  );

  $form['block']['imagef'] = array(
    '#type' => 'managed_file',
    '#title' => t('Choose Image'),
    '#upload_location' => 'public://ez_banner_rotator/',
    '#required' => TRUE,
    '#description' => t('Allowed filetype are "jpg", "gif", "png"') . '<br />' .
    t('Maximum allowed file size is') . ' 1MB',
    '#default_value' => !empty($ezr->fid) && $ezr->fid > 0 ? $ezr->fid : 0,
  );

  $form['block']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#size' => 60,
    '#default_value' => !empty($ezr->description) ? $ezr->description : '',
  );

  $form['block']['link_to'] = array(
    '#type' => 'textfield',
    '#title' => t('Link'),
    '#size' => 60,
    '#default_value' => !empty($ezr->link_to) ? $ezr->link_to : 'http://',
  );

  if ($arg2 > 0) {
    $form['block']['update'] = array(
      '#type' => 'submit',
      '#value' => t('Update Layer'),
    );
  }
  else {
    $form['block']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save Layer'),
    );
  }

  $form['block']['goback'] = array(
    '#markup' => '<a href="' .
    url('admin/structure/ez-banner-blocks/' . $arg1) . '" title="" class="button">Go Back</a>',
  );
  return $form;
}

/**
 * Implementation of ez_banner_rotator_get_form_add_banner_layer_validate().
 */
function ez_banner_rotator_get_form_add_banner_layer_validate($form, &$form_state) {
  $file = new stdClass();
  if ($form_state['values']['imagef'] != 0) {
    $file = file_load($form_state['values']['imagef']);
  }

  if (!file_validate_extensions($file, 'jpg,JPG,gif,GIF,png,PNG')) {
    form_set_error('imagef', 'Filetype Mismatch');
  }
}

/**
 * Implementation of ez_banner_rotator_get_form_add_banner_layer_submit().
 */
function ez_banner_rotator_get_form_add_banner_layer_submit($form, &$form_state) {
  global $user;

  $block_id = $form_state['values']['bid'];
  $banner_block_db = ez_banner_rotator_load_block($block_id);

  $image_id = 0;
  if (isset($form_state['values']['image_id'])) {
    $image_id = $form_state['values']['image_id'];
  }

  if ($form_state['values']['imagef'] != 0) {
    $file = file_load($form_state['values']['imagef']);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'ez_banner_rotator', 'ez_banner_rotator_save', 1);

    // Create destination for thumbnail image.
    $thumb_destination = 'public://ez_banner_rotator/thumb';
    if (!file_exists(drupal_realpath($thumb_destination))) {
      drupal_mkdir($thumb_destination, 0755, TRUE);
    }

    // Copy image file to thumbnail path.
    $file_thumb = file_copy($file, $thumb_destination);

    // Resizin for main page.
    $image = image_load($file->uri);
    $scaled = image_scale_and_crop($image, $banner_block_db->width, $banner_block_db->height);
    image_save($image);
    // Resizing for thumbnail page.
    $image = image_load($file_thumb->uri);
    $val = image_scale_and_crop($image, 100, 70);
    image_save($image);
  }

  $form_values = $form_state['values'];
  foreach ($form_values as $key => $values) {
    $form_values[$key] = check_plain($values);
  }

  $fields = array(
    'bid'      => $form_values['bid'],
    'uid'      => $user->uid,
    'title'    => $form_values['title'],
    'title'    => $form_values['title'],
    'fid'      => $file->fid,
    'thumb_fid'   => $file_thumb->fid,
    'link_to'     => $form_values['link_to'],
    'description' => $form_values['description'],
    'created_on'  => time(),
    'updated_on'  => time(),
  );

  $id = $num_updated = 0;

  if ($image_id > 0) {
    $num_updated = db_update('ez_banner_rotator')
      ->fields($fields)
      ->condition('image_id', $image_id, '=')
      ->execute();
  }
  else {
    $id = db_insert('ez_banner_rotator')
      ->fields($fields)
      ->execute();
  }
  if ($id > 0 || $num_updated > 0) {
    drupal_set_message(t('Image layer has been saved'));
  }
  drupal_goto('admin/structure/ez-banner-blocks/' . $form_values['bid']);
}
