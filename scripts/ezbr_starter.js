/**
 * Ez Banner Scripts
 * 2012/02/25
 */
(function ($) {

  Drupal.behaviors.ez_banner_rotator = {
    attach: function (context, settings) {
      ez_banner_rotator_start_banner(Drupal.settings.ez_banner_rotator);
    }
  };

})(jQuery);
