<?php

/**
 * @file
 * Install, update and uninstall functions for the EZ Banner Rotator module.
 */

/**
 * Implements hook_schema().
 */
function ez_banner_rotator_schema() {
  // ez_banner_rotator
  $schema['ez_banner_rotator'] = array(
    'description' => 'banner table for storing banner information',
    'fields' => array(
      'image_id' => array(
        'description' => 'Primary, Auto increment field for banner',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'bid' => array(
        'description' => 'block id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'description' => 'User who created',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'title' => array(
        'description' => 'Title for the banner image',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
        'translatable' => TRUE,
      ),
      'fid' => array(
        'description' => 'image file for the banner',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'thumb_fid' => array(
        'description' => 'thumbnail image id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'link_to' => array(
        'description' => 'Link to url',
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
      ),
      'description' => array(
        'description' => 'description for the banner',
        'type' => 'text',
        'not null' => FALSE,
        'translatable' => TRUE,
      ),
      'created_on' => array(
        'description' => 'Date created on',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'updated_on' => array(
        'description' => 'Date updated on',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('image_id'),
    'indexes' => array(
      'bid' => array('bid'),
      'fid' => array('fid'),
      'uid' => array('uid'),
    ),
  );

  // ez_banner_rotator_blocks
  $schema['ez_banner_rotator_blocks'] = array(
    'description' => 'EZ Banner Rotator blocks',
    'fields' => array(
      'bid' => array(
        'description' => 'primary key for the block',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => 'title of the block',
        'type' => 'varchar',
        'length' => '100',
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'description about banner block',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'width' => array(
        'description' => 'width in pixels',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 600,
      ),
      'height' => array(
        'description' => 'height in pixels',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 200,
      ),
      'num_images' => array(
        'description' => 'Number of images for the block',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 4,
      ),
      'other_options' => array(
        'description' => 'extra options',
        'type' => 'text',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('bid'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function ez_banner_rotator_install() {
  // Getting t() funciton.
  $t = get_t();
  // Default options.
  $options = array(
    'transition_type'     => 'slide',
    'auto_rotate'         => 1,
    'auto_rotate_timeout' => 5000,
    'enable_footer'       => 1,
    'auto_hide_footer'    => 1,
    'enable_thumb_image_link' => 1,
    'enable_title'        => 1,
    'enable_description'  => 1,
    'enable_readmore'     => 1,
  );

  // Insert default value for a default block.
  $id = db_insert('ez_banner_rotator_blocks')
      ->fields(array(
        'title'       => 'EZ Banner - Main',
        'description' => 'Main banner for the site',
        'width'       => 600,
        'height'      => 200,
        'num_images'  => 4,
        'other_options' => serialize($options),
      ))
      ->execute();

  if ($id) {
    drupal_set_message($t('Default block has been created. Please update the block and image layers.' .
      l($t('Edit default Block'), 'admin/structure/ez-banner-blocks/1/edit')));
  }
}
