<?php
/**
 * @file
 * Provide the default template for generating javascript object
 * to be processed by the ez_banner_rotator.js file
 * 10 December 2011
 */

/*
 * class EZBannerRotator
 */
class EZBannerRotator {
  // Variables.
  public $width = 600;
  public $height = 200;
  public $transitionType = 'slide';
  public $numOfImages = 4;
  public $autoRotate = TRUE;
  public $autoRotateTimeout = 5000;
  public $enableFooter = TRUE;
  public $autoHideFooter = TRUE;
  public $enableThumbImageLink = TRUE;
  public $enableTitle = TRUE;
  public $enableDescription = TRUE;
  public $enableReadmore = TRUE;
  // Variables : Arrays.
  public $images = array();
  public $titles = array();
  public $summary = array();
  public $linkUrl = array();
}
