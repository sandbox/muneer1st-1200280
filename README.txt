EZ_BANNER_ROTATOR.MODULE

http://www.encodez.com/ez-banner-rotator.html

Oveview
-------
EZ Banner Rotator is a JQuery based advanced banner module. 
This provide an Image based slideshow like banner rotator for Drupal pages. 
This creates blocks in Drupal which can be assigned to a page  
from within the Block administration.

This module extends the feature of Drupal to allow site administrator 
create rich looking javascript based banner rotator.

Requirments
-----------
* Drupal's Library API
* jquery.backgroundPosition.js
* jquery.easing.compatibility.js
* ez_banner_rotator.js

[ Instruction to download & install the above requirments are 
given below under the topic 'Installation'. ]

Installation
------------
* Copy ez_banner_rotator directory to your [/sites/all/modules] directory.
* Enable the module by navigating to admin/modules
* Update permissions by navigating to 
  admin/people/permissions#module-ez_banner_rotator
* Download and install Libraries API from Drupal site.
  http://drupal.org/project/libraries
* If you do not have a 'libraries' directory in your sites/all path,
  creat it. [sites/all/libraries]
* Add following 2 jquery plugins under sites/all/libraries/jquery_plugin
  1) jquery.backgroundPosition.js [v 1.22 or higher]
     - Files path should be 
       sites/all/libraries/jquery_plugin/jquery.backgroundPosition.js

  2) jquery.easing.compatibility.js
     URL to Download : http://gsgd.co.uk/sandbox/jquery/easing/
     - These plugins are not packed together with EZ Banner Rotator to 
       avoid conflict with other modules.
     - Files path should be 
       sites/all/libraries/jquery_plugin/jquery.easing.compatibility.js

  URL to Download both jquery_plugin:
  (Use the below URL to download both  files if you are not able to download
   from above URLs)
  http://www.encodez.com/downloads/jquery_plugin.zip

* Download ez_banner_rotator.js from following location
  http://www.encodez.com/downloads/ez_banner_rotator.js.zip
  - Unzip the file to same as the following path
  sites/all/libraries/ez_banner_rotator/ez_banner_rotator.js

Usage
-----
* Configure the block by navigating to admin/structure/ez-banner-blocks
  - The page would dispaly all the blocks in the table with following 
    operations
    1) "View"   - Views the block and the depending layers. You can 
                  update the layers individually here.
                  (upload image, create title, create description, create link)
    2) "Edit"   - Edit the properties of block.
                  * configure the size of block
                  * Number of images
                  * Rotation Style
                  * Other block options 
                    (navigate to the page to see more options.)
    3) "Delete" - Delete the block
* Assign created blocks using "Blocks" by navigating to 
  admin/structure/block
